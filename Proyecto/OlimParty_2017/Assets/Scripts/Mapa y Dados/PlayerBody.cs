﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PlayerBody : MonoBehaviour {

    

    public int spacesToMove;

    public Tile startingTile;//casilla en la que estamos al inicio del turno

    public Tile currentTile;

    public Tile finalTile;//casilla en la que acabamos al movernos

        
    bool casillaCheckeada = false; 

    Tile[] moveQueue;
    int moveQueueIndex;
    float smoothDistance = 0.5f; //(el tio tenia puesto 0.01f)

    bool scoreMe = false;

    //para la animacion
    Vector3 targetPosition;
    Vector3 velocity;
    float smoothTime = 1f;
    
    
    public static Vector3 posicionquesePasa = new Vector3 (-6.947f, 0.107f, -0.311f);
    static int chequearStart = 0;

    static int numeroCasilla = 0;
    public Tile[] todasLasCasillas = new Tile[61];

    public Tile[] casillasMinijuego = new Tile[20];
    public Tile[] casillasEvento = new Tile[20];
    public Tile[] casillasNoPasaNada = new Tile[21];




    
    // Use this for initialization
    void Start()
    {
        
        this.transform.position = posicionquesePasa;
        if (chequearStart < 2)
        {
            startingTile = todasLasCasillas[0];
            chequearStart++;
        }
        else
        {
            startingTile = todasLasCasillas[numeroCasilla];
        }

        
        
        targetPosition = this.transform.position;
        
        
    }

    // Update is called once per frame
    void Update()
    {

        if (Vector3.Distance(this.transform.position, targetPosition) < smoothDistance)
        {
            if (moveQueue != null && moveQueueIndex < moveQueue.Length)
            {
                Tile nextTile = moveQueue[moveQueueIndex];

                if (nextTile == null)
                {
                    //we are probably being scored
                    SetNewTargetPosition(this.transform.position + Vector3.right * 100f);
                    //aqui hay que ver qué hacer, estamos tirando la ficha hacia la derecha a toda ostia para que desaparezca

                }
                else
                {
                    SetNewTargetPosition(nextTile.transform.position);
                    moveQueueIndex++;
                    this.transform.Rotate(Vector3.up * 11.8f);

                }
            }
            else
            {
                //the movement queue is empty, so we are done animating!
                StateManager.IsDoneAnimating = true;

            }
        

        }

        this.transform.position = Vector3.SmoothDamp(this.transform.position, targetPosition, ref velocity, smoothTime);
        
            
        if (moveQueueIndex == spacesToMove && casillaCheckeada == false)
        {
            

            for (int i = 0; i<casillasMinijuego.Length; i++)
            {
                if (finalTile == casillasMinijuego[i])
                {
                    

                    Check(1);
                }
            }

            for (int i = 0; i < casillasNoPasaNada.Length; i++)
            {
                if (finalTile == casillasNoPasaNada[i])
                {
                    
                    Check(0);
                }
            }

            for (int i = 0; i < casillasEvento.Length; i++)
            {
                if (finalTile == casillasEvento[i])
                {
                    Check(2);
                }
            }
        }

    }

    

    //para la animacion 
    void SetNewTargetPosition(Vector3 pos)
    {
        targetPosition = pos;
        velocity = Vector3.zero;
    }

    void SetNewTargetRotation(Vector3 rot)
    {
        
    }


    //Qué pasa cuando clickamos
    void OnMouseUp()
    {

        if (StateManager.IsDoneClicking == true)
        {
            //we've already done a move! oh boi
            return;

        }

        //hemos tirado el dado?
        if (StateManager.IsDoneRolling == false)
        {
            //we cant move yet
        return;
        }

        spacesToMove = DiceRoller.DiceValuequesepasa;

        Debug.Log(" patata " + DiceRoller.DiceValuequesepasa);

        //where should we end up?

        moveQueue = new Tile[spacesToMove];

        finalTile = currentTile;

        

        for (int i = 0; i < spacesToMove; i++)
        {
            if (finalTile ==null && scoreMe == false) 
            {
                finalTile = startingTile; /* Aqui habria que hacer lo que pasa cuando llegamos al final */
            }
            else
            {
                if (finalTile.NextTiles == null || finalTile.NextTiles.Length == 0) 
                {
                    scoreMe = true;
                    finalTile = null;
                }

                else 
                {
                    finalTile = finalTile.NextTiles[0];
                }
               
            }

            moveQueue[i] = finalTile;

            
                
        }

        posicionquesePasa = finalTile.transform.position;
                
        numeroCasilla = numeroCasilla + DiceRoller.DiceValuequesepasa;

        Debug.Log(" numero casilla" + numeroCasilla);

        


        

        if (finalTile == null)
        {
            //we moved an off board tile by zero.
            return;
        }


        //TO DO ANIMATE
        //this.transform.position = finalTile.transform.position;
        moveQueueIndex = 0;
        currentTile = finalTile;

        StateManager.IsDoneClicking = true;

    }

    IEnumerator Minijuegos()
    {
        yield return new WaitForSeconds(4);
        SceneManager.LoadScene("Stack");
    }

    IEnumerator Eventos()
    {
        yield return new WaitForSeconds(4);
        
    }

    void Check(int tipo)
    {
        casillaCheckeada = true;
        switch (tipo)
        {
            //tipo nada
            case 0:
                {

                    Debug.Log("patata pero es 0");

                }
                break;

            //tipo minijuego
            case 1:
                {
                    Debug.Log("patata pero es 1");
                    StartCoroutine(Minijuegos());
                                
                }
                break;

            //tipo evento
            case 2:
                {
                    Debug.Log("patata pero es 2");
                }
                break;

        }

    }

}
