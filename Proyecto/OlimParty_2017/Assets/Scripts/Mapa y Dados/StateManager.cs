﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class StateManager : MonoBehaviour {


    public GameObject CurrentPlayerText;

    public static int NumberOfPlayers = 2; // DE MOMENTO
    public static int CurrentPlayerId = 0;
    int currentplayerdisplay;
    public static bool IsDoneRolling;
    public static bool IsDoneAnimating;
    public static bool IsDoneClicking;

    private void Start()
    {
        currentplayerdisplay = CurrentPlayerId + 1;
        CurrentPlayerText.GetComponent<Text>().text = "Current Player: " + currentplayerdisplay;
    }

    public void NewTurn()
    {

        IsDoneRolling = false;//Probablemente tengamos que usar statics de estas cosas
        IsDoneAnimating = false;
        IsDoneClicking = false;

        CurrentPlayerId = (CurrentPlayerId + 1) % NumberOfPlayers;
        
    }


    void Update()
    {
        if (IsDoneRolling && IsDoneClicking && IsDoneAnimating)
        {
            NewTurn();
        }
    }
}
