﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DiceTotalDisplay : MonoBehaviour {

    DiceRoller theDiceRoller;
    

    // Use this for initialization
    void Start () {
        theDiceRoller = GameObject.FindObjectOfType<DiceRoller>();
	}
	
	// Update is called once per frame
	void Update () {
        if (StateManager.IsDoneRolling== false) {
            GetComponent<Text>().text = "= ?";
        }
        else
        {
            GetComponent<Text>().text = "= " +theDiceRoller.DiceValue;

        }
        
    }
}
